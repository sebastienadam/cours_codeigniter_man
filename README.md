Ce projet contient les fichiers Latex servant à la génération de mon tutoriel
"Réalisez votre blog avec le framework CodeIgniter 3".

Pour plus de détails sur ce tutoriel, rendez-vous sue mon [site personnel](https://www.sebastienadam.be/connaissances/cours/realisez_votre_blog_avec_codeigniter_3.php).