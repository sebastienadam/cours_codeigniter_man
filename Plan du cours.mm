<map version="1.1.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node COLOR="#000000" CREATED="1448115042188" ID="ID_973328897" MODIFIED="1448115303440" TEXT="Cours CodeIgniter">
<font NAME="SansSerif" SIZE="20"/>
<hook NAME="accessories/plugins/AutomaticLayout.properties"/>
<node COLOR="#0033ff" CREATED="1448115340966" ID="ID_610224683" MODIFIED="1467362969650" POSITION="right" TEXT="3. Le blog">
<edge STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<icon BUILTIN="button_ok"/>
<node COLOR="#00b439" CREATED="1465324218877" ID="ID_1409053059" MODIFIED="1467279333184" TEXT="Introduction">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<icon BUILTIN="button_ok"/>
</node>
<node COLOR="#00b439" CREATED="1465324223286" ID="ID_189122931" MODIFIED="1467279336999" TEXT="Base de donn&#xe9;es">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<icon BUILTIN="button_ok"/>
</node>
<node COLOR="#00b439" CREATED="1465324239182" ID="ID_682530044" MODIFIED="1467293025836" TEXT="Liste des articles">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<icon BUILTIN="button_ok"/>
<node COLOR="#990000" CREATED="1465324253360" ID="ID_1501210533" MODIFIED="1467293025836" TEXT="Mod&#xe8;le">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
<node COLOR="#990000" CREATED="1465324256084" ID="ID_680394078" MODIFIED="1467293025836" TEXT="Vue">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
<node COLOR="#990000" CREATED="1465324247966" ID="ID_1976825829" MODIFIED="1467293025835" TEXT="Contr&#xf4;leur">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
<node COLOR="#990000" CREATED="1466535920300" ID="ID_412306293" MODIFIED="1467293025835" TEXT="Routage">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1448115412826" ID="ID_1569323375" MODIFIED="1467293025829" TEXT="Poster un article">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<icon BUILTIN="button_ok"/>
<node COLOR="#990000" CREATED="1465324303959" ID="ID_1929823047" MODIFIED="1467293025834" TEXT="Mod&#xe8;le">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
<node COLOR="#990000" CREATED="1465324307797" ID="ID_167946221" MODIFIED="1467293025833" TEXT="Vue">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
<node COLOR="#990000" CREATED="1465324247966" ID="ID_14056878" MODIFIED="1467293025833" TEXT="Contr&#xf4;leur">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1448115379492" ID="ID_816589288" MODIFIED="1467293595820" TEXT="Afficher un article">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<icon BUILTIN="button_ok"/>
<node COLOR="#990000" CREATED="1448115393762" ID="ID_985170226" MODIFIED="1467293595824" TEXT="Mod&#xe8;le">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
<node COLOR="#990000" CREATED="1466705461719" ID="ID_14887369" MODIFIED="1467293595826" TEXT="Contr&#xf4;leur">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
<node COLOR="#990000" CREATED="1466705475061" ID="ID_65456677" MODIFIED="1467293595825" TEXT="Routage">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
<node COLOR="#990000" CREATED="1448115406758" ID="ID_989288229" MODIFIED="1467293595824" TEXT="Vue">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1448115448126" ID="ID_1604606811" MODIFIED="1467294762149" TEXT="Modifier un article">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<icon BUILTIN="button_ok"/>
<node COLOR="#990000" CREATED="1466777503527" ID="ID_252532613" MODIFIED="1467294762149" TEXT="Vue">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
<node COLOR="#990000" CREATED="1466777495666" ID="ID_829708146" MODIFIED="1467294762145" TEXT="Contr&#xf4;leur">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1448115452892" ID="ID_927666432" MODIFIED="1467362960194" TEXT="Supprimer un article">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<icon BUILTIN="button_ok"/>
<node COLOR="#990000" CREATED="1465324303959" ID="ID_462946656" MODIFIED="1467362960189" TEXT="Mod&#xe8;le">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
<node COLOR="#990000" CREATED="1465324307797" ID="ID_724351407" MODIFIED="1467362960193" TEXT="Vue">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
<node COLOR="#990000" CREATED="1465324247966" ID="ID_412030028" MODIFIED="1467362960193" TEXT="Contr&#xf4;leur">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
<node COLOR="#990000" CREATED="1467038618480" ID="ID_449649233" MODIFIED="1467362960194" TEXT="AJAX">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1448115361771" ID="ID_1171910590" MODIFIED="1467036048188" TEXT="Les commentaires">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<icon BUILTIN="button_cancel"/>
</node>
<node COLOR="#00b439" CREATED="1467318399941" ID="ID_41042078" MODIFIED="1467362960194" TEXT="Conclusion">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<icon BUILTIN="button_ok"/>
</node>
</node>
<node COLOR="#0033ff" CREATED="1448179745398" ID="ID_1962629865" MODIFIED="1467030932729" POSITION="right" TEXT="4. Les petits plus">
<edge STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<icon BUILTIN="button_cancel"/>
<node COLOR="#00b439" CREATED="1448185033709" ID="ID_34373157" MODIFIED="1467030959337" TEXT="La gestion des erreurs">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<icon BUILTIN="button_cancel"/>
</node>
<node COLOR="#00b439" CREATED="1448185040620" ID="ID_471048976" MODIFIED="1467030962673" TEXT="Les environnements">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<icon BUILTIN="button_cancel"/>
</node>
<node COLOR="#00b439" CREATED="1448396251519" ID="ID_1753492934" MODIFIED="1467030967417" TEXT="Les formulaires sans Capcha">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<icon BUILTIN="button_cancel"/>
</node>
<node COLOR="#00b439" CREATED="1466536056268" ID="ID_1613105390" MODIFIED="1467030969816" TEXT="Redirection apr&#xe8;s authentification">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<icon BUILTIN="button_cancel"/>
</node>
<node COLOR="#00b439" CREATED="1448385296663" ID="ID_133222522" MODIFIED="1467030974433" TEXT="Adaptation de CodeIgniter">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<icon BUILTIN="button_cancel"/>
</node>
</node>
<node COLOR="#0033ff" CREATED="1448115065991" ID="ID_1442776701" MODIFIED="1467277428466" POSITION="left" TEXT="Pr&#xe9;sentation">
<edge STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<icon BUILTIN="button_ok"/>
<node COLOR="#00b439" CREATED="1448369035233" ID="ID_1056584797" MODIFIED="1467277428460" TEXT="Pourquoi ce cours?">
<edge STYLE="bezier" WIDTH="thin"/>
<font ITALIC="true" NAME="SansSerif" SIZE="16"/>
<icon BUILTIN="button_ok"/>
</node>
<node COLOR="#00b439" CREATED="1448115099090" ID="ID_771957310" MODIFIED="1467277428464" TEXT="Pr&#xe9;-requis">
<edge STYLE="bezier" WIDTH="thin"/>
<font ITALIC="true" NAME="SansSerif" SIZE="16"/>
<icon BUILTIN="button_ok"/>
</node>
<node COLOR="#00b439" CREATED="1448115093520" ID="ID_1716923581" MODIFIED="1467277428464" TEXT="Public cible">
<edge STYLE="bezier" WIDTH="thin"/>
<font ITALIC="true" NAME="SansSerif" SIZE="16"/>
<icon BUILTIN="button_ok"/>
</node>
<node COLOR="#00b439" CREATED="1448115122929" ID="ID_1751955368" MODIFIED="1467277428465" TEXT="But du cous">
<edge STYLE="bezier" WIDTH="thin"/>
<font ITALIC="true" NAME="SansSerif" SIZE="16"/>
<icon BUILTIN="button_ok"/>
</node>
</node>
<node COLOR="#0033ff" CREATED="1448115136851" ID="ID_1974591107" MODIFIED="1467277455352" POSITION="left" TEXT="1. Concepts de base">
<edge STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<icon BUILTIN="button_ok"/>
<node COLOR="#00b439" CREATED="1448115271693" ID="ID_465537348" MODIFIED="1467277455364" TEXT="Le mod&#xe8;le MVC">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<icon BUILTIN="button_ok"/>
<node COLOR="#990000" CREATED="1448116274529" ID="ID_1677218764" MODIFIED="1467277455362" TEXT="Qu&apos;est-ce que MVC?">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
<node COLOR="#990000" CREATED="1448116286377" ID="ID_649395794" MODIFIED="1467277455362" TEXT="Le contr&#xf4;leur">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
<node COLOR="#990000" CREATED="1448116293773" ID="ID_1413346369" MODIFIED="1467277455362" TEXT="La vue">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
<node COLOR="#990000" CREATED="1448116297124" ID="ID_405455120" MODIFIED="1467277455363" TEXT="Le Mod&#xe8;le">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
<node COLOR="#990000" CREATED="1448369010855" ID="ID_1459777342" MODIFIED="1467277455363" TEXT="Conclusion">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1448115159242" ID="ID_101630857" MODIFIED="1467277455361" TEXT="Framework">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<icon BUILTIN="button_ok"/>
<node COLOR="#990000" CREATED="1448115713934" ID="ID_1296178488" MODIFIED="1467277455361" TEXT="Qu&apos;est-ce qu&apos;un framework">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
<node COLOR="#990000" CREATED="1448116030344" HGAP="29" ID="ID_102891340" MODIFIED="1467277455361" TEXT="Pr&#xe9;sentation de CI">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1448115278497" ID="ID_1994147930" MODIFIED="1467277455359" TEXT="Installation">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<icon BUILTIN="button_ok"/>
<node COLOR="#990000" CREATED="1448115795877" ID="ID_1497452158" MODIFIED="1467277455360" TEXT="Installation standard">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
<node COLOR="#990000" CREATED="1448115802107" ID="ID_1974789338" MODIFIED="1467277455360" TEXT="Les fichiers de configuration">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
<node COLOR="#990000" CREATED="1448115811183" ID="ID_869985727" MODIFIED="1467277455360" TEXT="Installation personnalis&#xe9;e">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1448115258542" ID="ID_1038096571" MODIFIED="1467277455359" TEXT="La page d&apos;accueil">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<icon BUILTIN="button_ok"/>
<node COLOR="#990000" CREATED="1448115607442" ID="ID_716330996" MODIFIED="1467277455357" TEXT="Le routage">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
<node COLOR="#990000" CREATED="1448115611570" ID="ID_431160485" MODIFIED="1467277455357" TEXT="Le contr&#xf4;leur">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
<node COLOR="#990000" CREATED="1448115621381" ID="ID_679752535" MODIFIED="1467277455358" TEXT="La vue">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
<node COLOR="#990000" CREATED="1448116694902" ID="ID_447353189" MODIFIED="1467277455358" TEXT="Le helper URL">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
<node COLOR="#990000" CREATED="1448456192687" ID="ID_1265225362" MODIFIED="1467277455358" TEXT="Le Helper HTML">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1448115264693" ID="ID_1460853940" MODIFIED="1467277455352" TEXT="La page de contact">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<icon BUILTIN="button_ok"/>
<node COLOR="#990000" CREATED="1449249514999" ID="ID_685833388" MODIFIED="1467277455357" TEXT="La page">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
<node COLOR="#990000" CREATED="1448115654008" ID="ID_189562801" MODIFIED="1467277455356" TEXT="Le formulaire">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
<node COLOR="#990000" CREATED="1448115660026" ID="ID_1932551918" MODIFIED="1467277455356" TEXT="Validation des donn&#xe9;es">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
<node COLOR="#990000" CREATED="1452845039403" ID="ID_138402469" MODIFIED="1467277455356" TEXT="Affichage des erreurs">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
<node COLOR="#990000" CREATED="1448116594761" ID="ID_1181146395" MODIFIED="1467277455355" TEXT="L&apos;envoi du mail">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1448115742141" ID="ID_1300602455" MODIFIED="1467277455351" TEXT="Exercices">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<icon BUILTIN="button_ok"/>
<node COLOR="#990000" CREATED="1448116356294" ID="ID_124517271" MODIFIED="1467277455351" TEXT="Cr&#xe9;er une page &quot;&#xc0; propos&quot;">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
<node COLOR="#990000" CREATED="1448116369951" ID="ID_1395846402" MODIFIED="1467277455348" TEXT="Confirmation e-mail page de contact">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
</node>
</node>
<node COLOR="#0033ff" CREATED="1448115283076" ID="ID_1241486998" MODIFIED="1467278320858" POSITION="left" TEXT="2. L&apos;authentification">
<edge STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<icon BUILTIN="button_ok"/>
<node COLOR="#00b439" CREATED="1464979545828" ID="ID_1615772528" MODIFIED="1467277480996" TEXT="Introduction">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<icon BUILTIN="button_ok"/>
</node>
<node COLOR="#00b439" CREATED="1448115294455" ID="ID_1144160419" MODIFIED="1467277480999" TEXT="Les sessions">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<icon BUILTIN="button_ok"/>
<node COLOR="#990000" CREATED="1448116404298" ID="ID_588983040" MODIFIED="1467277481004" TEXT="Le syst&#xe8;me de PHP">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
<node COLOR="#990000" CREATED="1448115470955" ID="ID_1738251690" MODIFIED="1467277481003" TEXT="Le syst&#xe8;me de CI">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1448115298416" ID="ID_1597122424" MODIFIED="1467277481000" TEXT="La base de donn&#xe9;es">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<icon BUILTIN="button_ok"/>
<node COLOR="#990000" CREATED="1465128672188" ID="ID_1431120675" MODIFIED="1467277481003" TEXT="Configuration">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
<node COLOR="#990000" CREATED="1448116956573" ID="ID_895484244" MODIFIED="1467277481003" TEXT="Le query builder">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
<node COLOR="#990000" CREATED="1448115498806" ID="ID_667683916" MODIFIED="1467277481002" TEXT="R&#xe9;cup&#xe9;ration des donn&#xe9;es">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1465128700242" ID="ID_1045915214" MODIFIED="1467277481000" TEXT="L&apos;impl&#xe9;mentation">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<icon BUILTIN="button_ok"/>
<node COLOR="#990000" CREATED="1465128719107" ID="ID_1095164524" MODIFIED="1467277481002" TEXT="Table des utilisateurs">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
<node COLOR="#990000" CREATED="1448115638411" ID="ID_261161782" MODIFIED="1467277481002" TEXT="Le mod&#xe8;le">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
<node COLOR="#990000" CREATED="1448115326270" ID="ID_538147927" MODIFIED="1467277481001" TEXT="L&apos;authentification">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
<node COLOR="#990000" CREATED="1448115676278" ID="ID_608314266" MODIFIED="1467277481001" TEXT="La d&#xe9;connexion">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
<node COLOR="#990000" CREATED="1448115681126" ID="ID_1180807699" MODIFIED="1467277481000" TEXT="Le contr&#xf4;le des acc&#xe8;s">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1465156592155" ID="ID_578372984" MODIFIED="1467278320862" TEXT="Exercice">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<icon BUILTIN="button_ok"/>
<node COLOR="#990000" CREATED="1465156615993" ID="ID_426855716" MODIFIED="1467278320862" TEXT="Ajouter un panneau de contr&#xf4;le">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1465156599491" ID="ID_1144259492" MODIFIED="1467278320862" TEXT="Conclusion">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<icon BUILTIN="button_ok"/>
</node>
</node>
</node>
</map>
